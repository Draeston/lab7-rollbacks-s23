import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
inventory_size = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
add_inventory = (
    "INSERT INTO Inventory(username, product, amount) "
    "VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) "
    "DO UPDATE SET amount = Inventory.amount + EXCLUDED.amount"
)


def get_connection():
    return psycopg2.connect(
        dbname="lab7",
        user="postgres",
        password="postgres",
        host="localhost",
        port=5432
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            # Send all queries in one commit
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
                
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
                
                # Firstly, check if amount of items exceeds inventory size limit.
                # Then if items are withing inventory size - add item in inventory
                cur.execute(inventory_size, obj)
                res = cur.fetchone()
                current_amount = 0 if (res is None or res[0] is None) else res[0]
                if current_amount + amount > 100:
                    raise Exception("Limit size of the inventory is 100 items")
                
                cur.execute(add_inventory, obj)

                conn.commit()
            # If any query fails, then rollback whole transaction
            except Exception as e:
                conn.rollback()
                raise e



buy_product('Alice', 'marshmello', 1)